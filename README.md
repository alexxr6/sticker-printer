A Telegram bot which print stickers on a ticket printer.

The script uses lpr to send prints to the printer.

The script require these modules : 
pip install pyTelegramBotAPI dotenv

To create the database you can execute the sqlite queries on the initialize_sqlite.txt file