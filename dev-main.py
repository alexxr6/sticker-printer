from ast import And
import telebot # to use the Telegram API
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
import requests # to get image from the web
import shutil # to save it locally
import os # to use Shell commands
from dotenv import load_dotenv
from pathlib import Path
import subprocess # to use Shell commands
import cv2 # to use the webcam
import sqlite3 #To use the sqlite database
from datetime import datetime, timedelta

# Load .env variables
dotenv_path = Path('.env.dev')
load_dotenv(dotenv_path=dotenv_path)
# apiKey given by the bot
apiKey = os.getenv('apiKey')
# printer name on CUPS
printer = os.getenv('printer')
# database name
database = os.getenv('database')

# Wipe all datas that need to be wiped during the non NSFW events
## Connect the SQLite Database
connexion = sqlite3.connect(database)
## Create the cursor
curseur = connexion.cursor()
curseur.execute("SELECT s.id FROM stats s LEFT JOIN event	e ON s.wipe_asked  = e.id WHERE e.date_end < (SELECT DATETIME('now', '-7 day'))")
result = curseur.fetchall()
for row in result :
  curseur.execute("DELETE FROM stats WHERE id = ?", (row[0], ))
  connexion.commit()
  curseur.execute("DELETE FROM printing WHERE userID = ?", (row[0], ))
  connexion.commit()
curseur.close()
connexion.close()

# Define the bot
bot = telebot.TeleBot(apiKey, parse_mode=None)

# Help
@bot.message_handler(commands=['help'])
def sendHelp(message):
  helpmessage=f"""
Envoyer un sticker pour l'imprimer.

Commandes disponibles : 
/help : Affiche ces message
/stats : Affiche les statistiques globales, journalières et de l'utilisateur
/rgpd : Envoi un récapitulatif des données personnelles stockées
/wipe : Supprime toutes les données relatives à votre compte Telegram
"""
  bot.send_message(message.chat.id, helpmessage)


# Sticker Printing
@bot.message_handler(content_types=['sticker'])
def printingDocument(message):
  ## Get Telegram Variables
  emoji = message.sticker.emoji
  name = message.from_user.first_name
  id = message.from_user.id
  username = message.from_user.username
  language = message.from_user.language_code
  isAnimated = message.sticker.is_animated
  isVideo = message.sticker.is_video

  ## Connect the SQLite Database
  connexion = sqlite3.connect(database)
  ## Create the cursor
  curseur = connexion.cursor()
  
  ## Check if user is new
  curseur.execute("SELECT count(*) FROM stats WHERE id = ?", (id,))
  result = curseur.fetchone()
  presence = result[0]
  if presence == 0 :
    ### Define the date_creation value
    dateCreation = datetime.now().strftime("%Y-%m-%d %H:%M:%S.000")
    ### Create the user
    curseur.execute("INSERT INTO stats (id, username, pseudo, print, date_creation, date_last_use, news_read, language) VALUES (?, ?, ?, ?, ?, ?, 1, ?)", (id, username, name, 0, dateCreation, dateCreation, language))
    connexion.commit()
    ### Show the welcome message to the new user
    bot.send_message(message.chat.id, "Nouvel Utilisateur : Tapez /help pour voir la liste des commandes")

  ## Show news
  ## Get news_read value from database
  curseur.execute("SELECT news_read FROM stats WHERE id = ?", (id,))
  result = curseur.fetchone()
  newsRead = result[0]
  if newsRead == 0 :
    ### Get news title from the news list sorted from new to old
    curseur.execute("SELECT title FROM news WHERE activated = 1 ORDER BY id DESC LIMIT 1")
    result = curseur.fetchone()
    newsTitle = result[0]
    ### Get news content with the same sorting system
    curseur.execute("SELECT content FROM news WHERE activated = 1 ORDER BY id DESC LIMIT 1")
    result = curseur.fetchone()
    newsContent = result[0]
    ### Aggregate Title and Content with new lines
    news=str(newsTitle) + "\n\n" + str(newsContent)
    bot.send_message(message.chat.id, news)
    ### Update the news_read status on the database as it was displayed
    curseur.execute("UPDATE stats SET news_read = 1 WHERE id = ?", (id, ))
    connexion.commit()

  ## NSFW ToS
  ## Check if there is an event that allow NSFW
  curseur.execute("SELECT count(*), id FROM event WHERE nsfw = 0 AND date_start < CURRENT_TIMESTAMP AND date_end > CURRENT_TIMESTAMP")
  result = curseur.fetchone()
  isNotNSFW = result[0]
  idEvent = result[1]
  ## If this event is not NSFW
  if isNotNSFW == 1 :
    ### Check if the user has seen the Terms of Services
    curseur.execute("SELECT last_tos_read FROM stats WHERE id = ?", (id, ))
    result = curseur.fetchone()
    lastTosRead = result[0]
    ### If the last event ID where the TOS has been read is lower that the actual event
    if lastTosRead < idEvent :
      #### Print the Terms of Services
      termsOfServices=f"""Cet évènement n'autorise pas le contenu nsfw, merci de ne pas en imprimer.
Toutes les impressions seront sauvegardées pendant 7 jours en cas de plainte.
Vous pouvez toujours utiliser la commande RGPD, cependant les données ne seront supprimées qu'après 7 jours.
"""
      bot.send_message(message.chat.id, termsOfServices)

      #### Acknowledge
      #### Define options
      def gen_markup():
        markup = InlineKeyboardMarkup()
        markup.row_width = 2
        markup.add(InlineKeyboardButton("Yes", callback_data="cb_yes"),
        InlineKeyboardButton("No", callback_data="cb_no"))
        return markup

      #### Define the result and actions
      @bot.callback_query_handler(func=lambda call: True)
      def callback_query(call):
        ## Connect the SQLite Database
        connexion = sqlite3.connect(database)
        ## Create the cursor
        curseur = connexion.cursor()
        ##### Show the bot acknowledge
        ##### User agree
        if call.data == "cb_yes":
          ###### Print message
          bot.answer_callback_query(call.id, "Règlement accepté")
          ###### Store it on the database
          curseur.execute("UPDATE stats SET last_tos_read = ? WHERE id = ?;", (idEvent, id))
          connexion.commit()
        ##### User disagree
        elif call.data == "cb_no":
          bot.answer_callback_query(call.id, "Règlement refusé")

      #### Send the message
      bot.send_message(message.chat.id, "Accepter ou Refuser?", reply_markup=gen_markup())
      curseur.close()
      connexion.close()
      return  

  ## Update name if older than 24h to get the new Name if it was changed from the last use
  oneDayAgo = datetime.now() - timedelta(days = 1)
  oneDayAgo = oneDayAgo.strftime("%Y-%m-%d %H:%M:%S.000")
  curseur.execute("SELECT count(*) FROM stats WHERE id = ? AND date_last_use  < ?", (id, oneDayAgo))
  result = curseur.fetchone()
  olderThanADay = result[0]
  if olderThanADay > 0 :
    curseur.execute("UPDATE stats SET pseudo = ? WHERE id = ?", (name, id))
    connexion.commit()

  ## Check if animated or video
  ## Animated
  if str(isAnimated) == "True" or str(isVideo) == "True" :
    bot.send_message(message.chat.id, "Impression d'un sticker animé impossible")
  ## Static
  else :
    ### Check printer Queue
    ####queue = os.popen('lpq -P ' + printer + ' | grep "active" | wc -l').read().replace('\n','')
    queue = "0"
    ### If Queue not empty
    if queue != "0" :
      print(name +' : Une impression est déjà en cours, veuillez réessayer plus tard ')    
      bot.send_message(message.chat.id, "Une impression est déjà en cours, veuillez réessayer plus tard")
    else:
      dateLastUse = datetime.now().strftime("%Y-%m-%d %H:%M:%S.000")
      ### Get prints number
      curseur.execute("SELECT print FROM stats WHERE id = ?", (id,))
      result = curseur.fetchone()
      impressions = result[0]
      ### Increase it by 1
      impressions = impressions + 1
      ### And update the database
      curseur.execute("UPDATE stats SET pseudo = ?, print = ?, date_last_use = ? WHERE id = ?", (name, impressions, dateLastUse, id))
      connexion.commit()

      ### Add sticker to the stat database
      curseur.execute("INSERT INTO printing (userID, emoji, date_print) VALUES (?, ?, ?)", (id, emoji, dateLastUse))
      connexion.commit()
    
      ### Get all information about the file sent to find where it is stored
      file_info = bot.get_file(message.sticker.file_id)
      r = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(apiKey, file_info.file_path))
      ### Print URL for debug
      print('https://api.telegram.org/file/bot{0}/{1}'.format(apiKey, file_info.file_path))
      ### We check if the sticker picture is reachable
      if r.status_code == 200:
        #### We check if we are on a non NSFW event
        print(isNotNSFW)
        if isNotNSFW == 1 :
          path = "events/" + str(idEvent)
          ##### Check if the folder with the event id exist to store the sticker
          isEventFolderExist = os.path.exists(path)
          ##### If the folder does not exist, we create it
          if not isEventFolderExist :
            os.makedirs(path)
          ##### We save the sticker on this folder as $id-$print
          with open(path + "/" + str(id) + "-" + str(impressions) + ".webp",'wb') as f :
            f.write(r.content)

        #### We save the sticker picture as "sticker.webp" on the script root folder
        with open("sticker.webp",'wb') as f:
          f.write(r.content)
          ##### Alert the user that the printing process has been lauched
          print(name + " : Impression en cours ...")
          bot.send_message(message.chat.id, "Impression en cours ...")
          ##### Convert the webp to a png
          ffmpeg = subprocess.Popen(('ffmpeg -y -hide_banner -loglevel error -i sticker.webp sticker.png'), shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
          ##### Don't forget to wait, else the printer won't wait ffmpeg
          ffmpeg.wait()
          ##### And now we sent the png to the printer
          ####lpr = subprocess.Popen(('lpr -P ' + printer + ' sticker.png'), shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
          ####lpr.wait()
      else:
        print('Image Couldn\'t be retreived')

  curseur.close()
  connexion.close()

# Stats
@bot.message_handler(commands=['stats'])
def sendStats(message):
  ## Get Telegram Variables
  id = message.from_user.id

  ## Define Date Variables
  dateStartDay = datetime.now().strftime("%Y-%m-%d 00:00:00.000")
  dateEndDay = datetime.now().strftime("%Y-%m-%d 23:59:59.999")

  ## Connect the SQLite Database
  connexion = sqlite3.connect(database)
  ## Create the cursor
  curseur = connexion.cursor()

  ## Gather informations
  ## TOTAL PRINTS FROM THE BEGINNING
  curseur.execute("SELECT count(*) FROM printing")
  result = curseur.fetchone()
  nbTotalPrintingGlobal = result[0]
  ## TOTAL PRINTS OF THE DAY
  curseur.execute("SELECT count(*) FROM printing WHERE date_print > ? AND date_print < ?", (dateStartDay, dateEndDay))
  result = curseur.fetchone()
  nbTotalPrintingDay = result[0]
  ## TOTAL PRINTS FOR THE GIVEN USER FROM THE BEGINNING
  curseur.execute("SELECT count(*) FROM printing WHERE userID = ?", (id, ))
  result = curseur.fetchone()
  nbTotalPrintingGlobalUser = result[0]
  ## TOTAL PRINTS FOR THE GIVEN USER OF THE DAY
  curseur.execute("SELECT count(*) FROM printing WHERE userID = ? AND date_print > ? AND date_print < ?", (id, dateStartDay, dateEndDay))
  result = curseur.fetchone()
  nbTotalPrintingDayUser = result[0]

  ## TOTAL USER FROM THE BEGINNING
  curseur.execute("SELECT count(*) FROM stats")
  result = curseur.fetchone()
  nbTotalUsersGlobal = result[0]
  ## TOTAL USER OF THE DAY
  curseur.execute("SELECT count(*) FROM stats WHERE date_last_use > ? AND date_last_use < ?", (dateStartDay, dateEndDay))
  result = curseur.fetchone()
  nbTotalUsersDay = result[0]

  ## TOP 5 EMOJI FROM THE BEGINNING
  curseur.execute("SELECT emoji, count(emoji) as count FROM printing p GROUP BY emoji ORDER BY count DESC LIMIT 5")
  topEmojiGlobal = curseur.fetchall()
  ## TOP 5 EMOJI OF THE DAY
  curseur.execute("SELECT emoji, count(emoji) as count FROM printing p WHERE date_print > ? AND date_print < ? GROUP BY emoji ORDER BY count DESC LIMIT 5", (dateStartDay, dateEndDay))
  topEmojiDay = curseur.fetchall()
  ## TOP 5 EMOJI OF THE GIVEN USER FROM THE BEGINNING
  curseur.execute("SELECT emoji, count(emoji) as count FROM printing p WHERE userID = ? GROUP BY emoji ORDER BY count DESC LIMIT 5", (id, ))
  topEmojiGlobalUser = curseur.fetchall()
  ## TOP 5 EMOJI OF THE GIVEN USER OF THE DAY
  curseur.execute("SELECT emoji, count(emoji) as count FROM printing p WHERE userID = ? AND date_print > ? AND date_print < ? GROUP BY emoji ORDER BY count DESC LIMIT 5", (id, dateStartDay, dateEndDay))
  topEmojiDayUser = curseur.fetchall()

  ## TOP 5 USERS FROM THE BEGINNING
  curseur.execute("SELECT pseudo, print FROM stats s ORDER BY print DESC LIMIT 5")
  topUsersGlobal = curseur.fetchall()
  ## TOP 5 USERS OF THE DAY
  curseur.execute("SELECT s.pseudo, count(p.userID) as count FROM printing p INNER JOIN stats s ON s.id = p.userID WHERE date_print > ? AND date_print < ? GROUP BY userID ORDER BY count DESC LIMIT 5", (dateStartDay, dateEndDay))
  topUsersDay = curseur.fetchall()

  ## Show GLOBAL datas
  ## Aggregate the emoji list to a full chain
  ## Getting length of topEmojiGlobal
  length = len(topEmojiGlobal)
  i = 0
  j = 1
  topEmojiGlobalAggregate = ""
  while i < length:
    topEmojiGlobalAggregate = topEmojiGlobalAggregate + "    " + str(j) + " : " + str(topEmojiGlobal[i][0]) + " x " + str(topEmojiGlobal[i][1]) + "\n"
    i += 1
    j += 1
    
  ## Aggregate the user list to a full chain
  ## Getting length of topUsersGlobal
  length = len(topUsersGlobal)
  i = 0
  j = 1
  topUsersGlobalAggregate = ""
  while i < length:
    topUsersGlobalAggregate = topUsersGlobalAggregate + "    " + str(j) + " : " + str(topUsersGlobal[i][0]) + " x " + str(topUsersGlobal[i][1]) + "\n"
    i += 1
    j += 1
    
  resultGlobal=f"""
Statistiques globales : 
  Nombre d'impressions : {str(nbTotalPrintingGlobal)}
  Nombre d'utilisateurs : {str(nbTotalUsersGlobal)}
  Emoji les plus utilisés : 
{topEmojiGlobalAggregate}
  Utilisateurs les plus actifs : 
{topUsersGlobalAggregate}
    """
  bot.send_message(message.chat.id, resultGlobal)

  ## Show DAILY datas
  ## Aggregate the emoji list to a full chain
  ## Getting length of topEmojiDay
  length = len(topEmojiDay)
  i = 0
  j = 1
  topEmojiDayAggregate = ""
  while i < length:
    topEmojiDayAggregate = topEmojiDayAggregate + "    " + str(j) + " : " + str(topEmojiDay[i][0]) + " x " + str(topEmojiDay[i][1]) + "\n"
    i += 1
    j += 1

  ## Aggregate the user list to a full chain
  ## Getting length of topUsersDay
  length = len(topUsersDay)
  i = 0
  j = 1
  topUsersDayAggregate = ""
  while i < length:
    topUsersDayAggregate = topUsersDayAggregate + "    " + str(j) + " : " + str(topUsersDay[i][0]) + " x " + str(topUsersDay[i][1]) + "\n"
    i += 1
    j += 1

  resultDay=f"""
Statistiques journalière : 
  Nombre d'impressions : {str(nbTotalPrintingDay)}
  Nombre d'utilisateurs : {str(nbTotalUsersDay)}
  Emoji les plus utilisés : 
{topEmojiDayAggregate}
  Utilisateurs les plus actifs : 
{topUsersDayAggregate}
    """
  bot.send_message(message.chat.id, resultDay)

  ## Show USER 
  ## Aggregate the emoji list to a full chain
  ## Getting length of topEmojiGlobalUser
  length = len(topEmojiGlobalUser)
  i = 0
  j = 1
  topEmojiGlobalUserAggregate = ""
  while i < length:
    topEmojiGlobalUserAggregate = topEmojiGlobalUserAggregate + "    " + str(j) + " : " + str(topEmojiGlobalUser[i][0]) + " x " + str(topEmojiGlobalUser[i][1]) + "\n"
    i += 1
    j += 1

  ## Aggregate the user list to a full chain
  ## Getting length of topEmojiDayUser
  length = len(topEmojiDayUser)
  i = 0
  j = 1
  topEmojiDayUserAggregate = ""
  while i < length:
    topEmojiDayUserAggregate = topEmojiDayUserAggregate + "    " + str(j) + " : " + str(topEmojiDayUser[i][0]) + " x " + str(topEmojiDayUser[i][1]) + "\n"
    i += 1
    j += 1

  ## IF User printed at least one sticker
  curseur.execute("SELECT count(*) FROM printing WHERE userID = ?", (id, ))
  result = curseur.fetchone()
  printExist = result[0]
  if printExist > 0 :
    resultUser=f"""
Statistiques Utilisateur : 
  Nombre d'impressions depuis le début : {str(nbTotalPrintingGlobalUser)}
  Nombre d'impression aujourd'hui : {str(nbTotalPrintingDayUser)}
  Emoji les plus utilisés depuis le début : 
{topEmojiGlobalUserAggregate}
  Emoji les plus utilisés aujourd'hui : 
{topEmojiDayUserAggregate}
    """
  else :
    resultUser = "Cet utilisateur n'a pas imprimé de stickers"

  bot.send_message(message.chat.id, resultUser)

  ## Close the database
  curseur.close()
  connexion.close()

# RGPD
# Send all user information in TXT file
@bot.message_handler(commands=['rgpd'])
def sendStats(message):
  ## Get Telegram Variables
  id = message.from_user.id

  ## Declare variable
  tableStatsDatasAggregate=""
  tablePrintingDatasAggregate=""
  datas=""

  ## Connect the SQLite Database
  connexion = sqlite3.connect(database)
  ## Create the cursor
  curseur = connexion.cursor()

  ## Check if user has data on Stats table
  curseur.execute("SELECT count(*) FROM stats WHERE id = ?", (id, ))
  result = curseur.fetchone()
  userExistOnStats = result[0]
  ## Check if user has data on Printing table
  curseur.execute("SELECT count(*) FROM printing WHERE userID = ?", (id, ))
  result = curseur.fetchone()
  userExistOnPrinting = result[0]

  ## If user does not have data
  if userExistOnStats == 0 & userExistOnPrinting == 0 :
    bot.send_message(message.chat.id, "Pas de données personnelles stockées sur le serveur")
  ## If user does have datas
  else :
    ### User has stats
    if userExistOnStats > 0 :
      #### Set column list of Stats table
      tableStatsColumn=["id", "username", "pseudo", "print", "date_creation", "date_last_use", "news_read", "language", "last_tos_read", "wipe_asked"]
      ### Get number of column
      tableStatsColumnLength=len(tableStatsColumn)

      #### Store column name on variable
      i=0
      tableStatsDatasAggregate="("
      while i < tableStatsColumnLength :
        tableStatsDatasAggregate=tableStatsDatasAggregate + str(tableStatsColumn[i]) + ", "
        i += 1
      tableStatsDatasAggregate=tableStatsDatasAggregate + ")\n"

      #### Get datas from table stats
      curseur.execute("SELECT * FROM stats WHERE id = ?", (id, ))
      tableStatsDatas = curseur.fetchall()

      #### Get number of raws
      tableStatsDatasLength = len(tableStatsDatas)
      i = 0
      while i < tableStatsDatasLength:
        tableStatsDatasAggregate=tableStatsDatasAggregate + str(tableStatsDatas[i]) + "\n"
        i += 1

    ### User has printing
    if userExistOnPrinting > 0 :
      #### Set column list of Printing table
      tablePrintingColumn=["id", "userID", "emoji", "date_print"]
      #### Get number of column
      tablePrintingColumnLength=len(tablePrintingColumn)

      #### Store column name on variable
      i=0
      tablePrintingDatasAggregate="("
      while i < tablePrintingColumnLength :
        tablePrintingDatasAggregate=tablePrintingDatasAggregate + str(tablePrintingColumn[i]) + ", "
        i += 1
      tablePrintingDatasAggregate=tablePrintingDatasAggregate + ")\n"

      #### Get datas from table printing
      curseur.execute("SELECT * FROM printing WHERE userID = ?", (id, ))
      tablePrintingDatas = curseur.fetchall()

      #### Get number of raws
      tablePrintingDatasLength = len(tablePrintingDatas)
      i = 0
      while i < tablePrintingDatasLength:
        tablePrintingDatasAggregate=tablePrintingDatasAggregate + str(tablePrintingDatas[i]) + "\n"
        i += 1
    
    ### Aggregate all datas
    datas="Données du compte : \n" + tableStatsDatasAggregate + "\nDonnées des impressions : \n" + tablePrintingDatasAggregate

    ### Create the TXT file
    f = open("rgpd_"+str(id)+".txt", "a")
    f.write(datas)
    f.close()

    ### Send it on the chat
    f = open("rgpd_"+str(id)+".txt", "rb")
    bot.send_document(message.chat.id, f)

    ### Remove the file
    os.remove("rgpd_"+str(id)+".txt")

  ## Close the database
  curseur.close()
  connexion.close()

# Wipe
# Wipe all datas attached to an user, and the user on the database
@bot.message_handler(commands=['wipe'])
def sendStats(message):
  ## Get Telegram Variables
  id = message.from_user.id

  ## Connect the SQLite Database
  connexion = sqlite3.connect(database)
  ## Create the cursor
  curseur = connexion.cursor()

  ## Check if user has data on Stats table
  curseur.execute("SELECT count(*) FROM stats WHERE id = ?", (id, ))
  result = curseur.fetchone()
  userExistOnStats = result[0]
  ## Check if user has data on Printing table
  curseur.execute("SELECT count(*) FROM printing WHERE userID = ?", (id, ))
  result = curseur.fetchone()
  userExistOnPrinting = result[0]

  ## If user does not have data
  if userExistOnStats == 0 & userExistOnPrinting == 0 :
    bot.send_message(message.chat.id, "Pas de données personnelles stockées sur le serveur")
  ## If user does have data
  else :
    ### Now check if we are on an non NSFW event to stock all datas for 7 days
    ### Check if there is an event that allow NSFW
    curseur.execute("SELECT count(*), id FROM event WHERE nsfw = 0 AND date_start < CURRENT_TIMESTAMP AND date_end > CURRENT_TIMESTAMP")
    result = curseur.fetchone()
    isNotNSFW = result[0]
    idEvent = result[1]
    ### If this event is not NSFW
#    if isNotNSFW == 1 and wipeAskedID != idEvent :
    if isNotNSFW == 1 :
      ### Check if there is already a event on wipe_asked
      curseur.execute("SELECT wipe_asked FROM stats WHERE id = ?", (id, ))
      result = curseur.fetchone()
      wipeAskedID = result[0]
      ### If user already asked
      if wipeAskedID == idEvent :
        bot.send_message(message.chat.id, "Vous avez déjà demandé une suppression de données")
      else :
        #### Store the event ID to determine the last day the data must be kept date_end + 7 days
        curseur.execute("UPDATE stats SET wipe_asked = ? WHERE id = ?", (idEvent, id))
        connexion.commit()
        bot.send_message(message.chat.id, "Vos données seront supprimées 7 jours après la fin de l'évènement")

    else :
      #### User has stats
      if userExistOnStats > 0 :
        ##### Delete all datas on the stats database
        curseur.execute("DELETE FROM stats WHERE id = ?", (id, ))
        connexion.commit()

      #### User has printing
      if userExistOnPrinting > 0 :
        ##### Delete all datas on the stats database
        curseur.execute("DELETE FROM printing WHERE userID = ?", (id, ))
        connexion.commit()
    
      bot.send_message(message.chat.id, "Données supprimées")

  ## Close the database
  curseur.close()
  connexion.close()

bot.polling()
